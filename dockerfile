#FROM ubuntu:latest
FROM tomcat

#add a maintainer line with your name
MAINTAINER Tim Wood

#update the image
run apt-get update -y
run apt-get upgrade -y

COPY target/scalatra-maven-prototype.war /usr/local/tomcat/webapps/hello-scalatra.war



CMD ["catalina.sh", "run"]


#open port 8080 to the outside world
expose 8080
